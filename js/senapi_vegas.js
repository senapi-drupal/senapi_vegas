(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.senapi_vegas = {
        attach: function (context, settings) {
            if (!context.activeElement) {
                return false;
            }

            var vegas = drupalSettings.vegas || [];
            if (vegas) {
                $('body', context).once('senapi_vegas').vegas(vegas);
            }
        }
    };
})(jQuery, Drupal, drupalSettings);
