<?php

namespace Drupal\senapi_vegas\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Class SenapiVegasBlock
 *
 * @Block(
 *   id = "senapi_vegas_block",
 *   admin_label = @Translation("Senapi Vegas Block"),
 *   category = @Translation("Content")
 * )
 * @package Drupal\senapi_vegas\Plugin\Block
 */
class SenapiVegasBlock extends BlockBase {

  /**
   * @inheritDoc
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $defaults = $this->defaultConfiguration();

    if ($form_state instanceof SubformStateInterface) {
      $ajax_values = $form_state->getCompleteFormState()->getValues();
    }
    else {
      $ajax_values = $form_state->getValues();
    }

    if (isset($ajax_values['settings']['senapi_vegas_settings']['content_selection']['entity_selected'])) {
      $config['entity_selected'] = $ajax_values['settings']['senapi_vegas_settings']['content_selection']['entity_selected'];
    }

    if (empty($config['entity_selected'])) {
      $config['entity_selected'] = $defaults['entity_selected'];
    }

    $form['senapi_vegas_settings'] = [
      '#type' => 'fieldset',
      '#title' => 'Senapi vegas configuration',
      '#descripton' => 'Configure the content selection and the slide fields used in senapi vegas.',
      '#attributes' => ['id' => 'senapi-vegas-wrapper'],
    ];

    $form['senapi_vegas_settings']['content_selection'] = [
      '#type' => 'details',
      '#title' => 'Content selection and ordering',
      '#description' => 'Filter, limit and sort the contents shwn on slides.',
      '#open' => TRUE,
      '#attributes' => ['id' => 'content-selection-wrapper'],
    ];

    $form['senapi_vegas_settings']['slide_fields'] = [
      '#type' => 'details',
      '#title' => 'Slide fields and styling',
      '#description' => 'Assign the field used as image on slides.',
      '#open' => TRUE,
      '#attributes' => ['id' => 'slide-fields-wrapper'],
    ];

    $form['senapi_vegas_settings']['content_selection']['entity_selected'] = [
      '#type' => 'select',
      '#title' => 'Entity type',
      '#default_value' => $config['entity_selected'],
      '#options' => $this->getEntityTypes(),
      '#description' => 'Check the entity that you want to use in the slides. Default: node',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxFormSettingsCallback'],
        'wrapper' => 'senapi-vegas-wrapper',
        'event' => 'change',
      ],
    ];

    $bundles = $this->getEntityTypeBundles($config['entity_selected']);
    if (!empty($bundles)) {
      $form['senapi_vegas_settings']['content_selection']['content_type'] = [
        '#type' => 'select',
        '#title' => 'Entity type bundles',
        '#default_value' => !empty($config['content_type']) ? $config['content_type'] : [],
        '#options' => $bundles,
        '#description' => '',
        '#prefix' => '<div class="form-type-radios">',
        '#sufix' => '</div>',
        '#attributes' => ['class' => ['container-inline']],
        '#validated' => TRUE,
      ];
    }

    $option_types = ['integer', 'created', 'changed', 'datetime'];
    $form['senapi_vegas_settings']['content_selection']['order_field'] = [
      '#type' => 'select',
      '#title' => 'Order by',
      '#options' => $this->getFieldByType($option_types, $config['entity_selected']),
      '#default_value' => $config['order_field'],
      '#empty_option' => '- None -',
      '#validated' => TRUE,
    ];


    $form['senapi_vegas_settings']['content_selection']['order_direction'] = [
      '#type' => 'select',
      '#title' => 'Order direction',
      '#options' => [
        'ASC' => 'Ascending',
        'DESC' => 'Descending',
        'RANDOM' => 'Random',
      ],
      '#default_value' => $config['order_direction'],
      '#states' => [
        'visible' => [
          ':input[name="settings[senapi_vegas_settings][content_selection][order_by]"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['senapi_vegas_settings']['content_selection']['limit'] = [
      '#type' => 'number',
      '#title' => 'Max number of elements',
      '#default_value' => $config['limit'],
      '#description' => 'The maximum number of elements to show in the slide.',
    ];

    $form['senapi_vegas_settings']['slide_fields']['slide_style'] = [
      '#type' => 'select',
      '#title' => 'Slide layout style',
      '#options' => [
        'vegas' => 'Vega Background',
      ],
      '#default_value' => $config['slide_style'],
      '#description' => 'The slide style vegas of visualization',
      '#required' => TRUE,
    ];

    $form['senapi_vegas_settings']['slide_fields']['image'] = [
      '#type' => 'select',
      '#title' => 'Image field',
      '#options' => $this->getFieldByType(['image'], $config['entity_selected']),
      '#default_value' => $config['image'],
      '#required' => TRUE,
    ];

    $form['senapi_vegas_settings']['slide_fields']['image_style'] = [
      '#type' => 'select',
      '#title' => 'Image style',
      '#options' => $this->getImageStyles(),
      '#default_value' => $config['image_style'],
      '#empty_option' => '- None -',
      '#validated' => TRUE,
    ];

    $form['senapi_vegas_settings']['slide_fields']['delay'] = [
      '#type' => 'number',
      '#title' => 'Slide delay',
      '#default_value' => (int) $config['delay'],
      '#min' => 0,
      '#description' => 'The amount of time (in ms) to delay between automatically cycling an item. If 0, slide will not automatically cycle.',
    ];

    $form['senapi_vegas_settings']['slide_fields']['overlay'] = [
      '#type' => 'number',
      '#title' => 'Slide overlay',
      '#default_value' => (int) $config['overlay'],
      '#min' => 0,
    ];

    return $form;
  }

  public function ajaxFormSettingsCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    return $form['settings']['senapi_vegas_settings'];
  }

  /**
   * @inheritDoc
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $config = $form_state->getValues();

    if (isset($config['senapi_vegas_settings'])) {
      $content_selection = [
        'entity_selected',
        'content_type',
        'order_field',
        'order_direction',
        'limit',
      ];

      foreach ($content_selection as $config_field) {
        if (isset($config['senapi_vegas_settings']['content_selection'][$config_field])) {
          $this->setConfigurationValue(
            $config_field,
            $config['senapi_vegas_settings']['content_selection'][$config_field]
          );
        }
      }

      $slide_fields = [
        'slide_style',
        'image',
        'image_style',
        'delay',
      ];

      foreach ($slide_fields as $config_field) {
        if (isset($config['senapi_vegas_settings']['slide_fields'][$config_field])) {
          $this->setConfigurationValue(
            $config_field,
            ($config_field == 'url') ? join('|', array_keys($config['senapi_vegas_settings']['slide_fields'][$config_field])) : $config['senapi_vegas_settings']['slide_fields'][$config_field]
          );
        }
      }

      Cache::invalidateTags(['config:block.block.senapi_vegas']);
    }
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    return [
      'entity_selected' => 'node',
      'content_type' => 'fondo',
      'image' => 'field_image',
      'image_style' => '',
      'order_field' => 'created',
      'order_direction' => 'DESC',
      'limit' => 5,
      'slide_style' => '',
      'delay' => 35000,
      'overlay' => 1,
      'timer' => FALSE,
      'shuffle' => FALSE,
      'firstTransition' => 'fade2',
      'transition' => 'fade2',
      'slidesToKeep' => 1,
    ];
  }


  /**
   * @inheritDoc
   */
  public function build() {
    $config = $this->getConfiguration();

    $settings['slides'] = $this->getSlides($config);
    $settings['delay'] = $config['delay'];
    $settings['overlay'] = $config['overlay'];
    $settings['timer'] = FALSE;
    $settings['shuffle'] = FALSE;

    $settings['firstTransition'] = 'fade2';

    $settings['transition'] = ['fade2'];
    $settings['slidesToKeep'] = 1;
    #$settings['animation'] = "random";

    return [
      '#type' => 'container',
      '#attached' => [
        'library' => ['senapi_vegas/vegas'],
        'drupalSettings' => [
          'vegas' => $settings,
        ],
      ],
    ];
  }

  private function getSlides($config) {
    $slides = [];
    $entities = $this->getQueriedEntities($config);

    if (!empty($entities)) {
      foreach ($entities as $entity) {
        $slides[] = $this->composeSlide($config, $entity);
      }
    }

    return $slides;
  }


  private function getEntityTypes() {
    $entities = [];
    $entity_definitions = Drupal::entityTypeManager()->getDefinitions();
    if (!empty($entity_definitions)) {
      foreach ($entity_definitions as $entity_definition) {
        if ($entity_definition instanceof ContentEntityType && $entity_definition->get('field_ui_base_route') && $entity_definition->id() == 'node') {
          $entities[$entity_definition->id()] = $entity_definition->id();
        }
      }
    }

    return $entities;
  }

  private function getEntityTypeBundles($entity) {
    $options = [];

    $entity_type = Drupal::entityTypeManager()
      ->getDefinition($entity)
      ->getBundleEntityType();
    if (!empty($entity_type)) {
      $entity_type_bundles = Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->loadMultiple();
      if (!empty($entity_type_bundles)) {
        foreach ($entity_type_bundles as $entity_type_bundle) {
          $options[$entity_type_bundle->id()] = $entity_type_bundle->label();
        }
      }
    }

    return $options;
  }

  private function getFieldByType($types, $entity = 'node') {
    $fields = $this->getFields($entity);

    $options = [];
    foreach ($types as $type) {
      if (isset($fields[$type])) {
        $options = array_merge($options, $fields[$type]);
      }
    }

    return $options;
  }

  private function getFields($entity_type = 'node', $grouped = TRUE) {


    $fields = Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entity_type);
    $options = [];

    foreach ($fields as $field) {
      if ($grouped) {
        $options[$field->getType()][$field->getName()] = $field->getLabel() . ' (' . $field->getName() . ')';
      }
      else {
        $options[$field->getName()] = $field->getLabel() . ' (' . $field->getName() . ')';
      }
    }

    return $options;
  }

  private function getImageStyles() {
    $styles = Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->loadMultiple();
    $options = [];
    foreach ($styles as $key => $style) {
      $options[$key] = $key;
    }

    return $options;
  }

  private function getQueriedEntities($config) {
    $entities = [];
    $entity_type = !empty($config['entity_selected']) ? $config['entity_selected'] : 'node';
    $storage = Drupal::entityTypeManager()->getStorage($entity_type);
    $query = $storage->getQuery();

    if (!empty($config['content_type'])) {
      $bundles = $this->getValidBundles($config['content_type'], $config['entity_selected']);
      $entity_keys = Drupal::entityTypeManager()
        ->getDefinition($entity_type)
        ->get('entity_keys');
      if (!empty($bundles) && isset($entity_keys['bundle'])) {
        $query->condition($entity_keys['bundle'], array_values($bundles), 'IN');
      }
    }

    $query->condition('status', 1);

    if (isset($config['order_direction']) && $config['order_direction'] == 'RANDOM') {
      $query->addTag('random_order');
    }
    elseif (!empty($config['order_field'])) {
      if ($config['order_direction'] == 'ASC' || $config['order_direction'] == 'DESC') {
        $query->sort($config['order_field'], $config['order_direction']);
      }
      else {
        $query->sort($config['order_field']);
      }
    }

    if (!empty($config['limit'])) {
      $query->range(0, $config['limit']);
    }

    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {
      $entities = $storage->loadMultiple($entity_ids);
    }

    return $entities;
  }

  private function getValidBundles($bundles, $entity) {
    $entityBundles = $this->getEntityTypeBundles($entity);
    $valid_bundles = [];
    if (isset($entityBundles[$bundles])) {
      $valid_bundles[] = $bundles;
    }


    return $valid_bundles;
  }

  private function composeSlide($config, $entity) {

    $image_width = $image_height = $image_uri = '';
    if (!empty($config['image']) && isset($entity->{$config['image']})) {
      $image_obj = $entity->{$config['image']}->entity;
      if (!empty($image_obj)) {
        $image_uri = $image_obj->getFileUri();
      }
      else {
        $default_image = $entity->{$config['image']}->getSetting('default_image');
        if (!empty($default_image) && isset($default_image['uuid'])) {
          $default_entity = Drupal::service('entity.repository')
            ->loadEntityByUuid('file', $default_image['uuid']);
          if (!empty($default_entity)) {
            $image_uri = $default_entity->getFileUri();
          }
        }
      }

      if (!empty($image_uri)) {
        if (!empty($config['image_style'])) {
          $style = ImageStyle::load($config['image_style']);
          $image_derivative = $style->buildUri($image_uri);

          if (!file_exists($image_derivative)) {
            $style->createDerivative($image_uri, $image_derivative);
          }
          $image_uri = $image_derivative;
        }
      }

      $image = Drupal::service('image.factory')->get($image_uri);
      if ($image->isValid()) {
        $image_width = $image->getWidth();
        $image_height = $image->getHeight();
      }
    }

    return [
      'src' => file_create_url($image_uri),
      'width' => $image_width,
      'height' => $image_height,
    ];
  }
}